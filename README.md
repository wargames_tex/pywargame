# Python utilities for Wargames 

This is a collection of various utilities to make things for Computer
Aided Wargaming (CAW). 

## Content

- [`common`](common) Common utilities used by other code.
- [`vassal`](vassal) Read or write [VASSAL](https://vassalengine.org)
  ([GitHub](https://github.com/vassalengine/vassal)) modules. 
- [`cyberboard`](cyberboard) Read
  [CyberBoard](http://cyberboard.norsesoft.com/)
  ([GitHub](https://github.com/CyberBoardPBEM/cbwindows)) GameBox
  files. 
- [`latex`](latex) Create (draft) VASSAL module from LaTeX sources
  using the package
  [`wargame`](https://gitlab.com/wargames_tex/wargame_tex). 
  

## Changes 

The code isn't versioned yet, and therefore the changes are given by
date. 

- 2nd of February, 2024
  - Fix problem with background of maps exported from CyberBoard
    gamebox.  Thanks to @zovs66 for pointing this out. 
  - `cyberboard.py` now supports file format version 4+ (but not 5, as
    that will introduce the saving for features and other stuff). 
    
## Scripts 

- [`gbxextract.py`](cyberboard/gbxext.py) reads in a CyberBoard
  GameBox file (`.gbx`) and writes out a ZIP file with images and a
  JSON file with information about the GameBox and the images. 
  
  Pieces (units and markers) are saved as PNGs, while the boards are
  saves as SVGs. 
  
- [`gsnextract.py`](cyberboard/gsnext.py) reads in a CyberBoard
  Scenario file (`.gsn`) and writes out a ZIP file with images and a
  JSON file with information about the Scenario, GameBox, and the
  images.
  
  Pieces (units and markers) are saved as PNGs, while the boards are
  saves as SVGs. 
  
- [`gsnexport.py`](cyberboard/gsnexp.py) reads in a
  CyberBoard Scenario file (`.gsn`) and generates a (draft) VASSAL
  module. A Python script can be supplied to patch up the module. 
  
- [`wgexport.py`](latex/main.py) reads in a PDF and JSON file created
  from LaTeX sources using the
  [`wargame`](https://gitlab.com/wargames_tex/wargame_tex) package,
  and generates a (draft) VASSAL module. A Python script can be
  supplied to patch up the module.

## Download 

You can get the scripts in ZIP file 

- [artifacts.zip][]

or individually 

- [vassal.py][]
- [cyberboard.py][]
- [gbxextract.py][]
- [gsnextract.py][]
- [gsnexport.py][]
- [wgexport.py][]
- [requirements.txt][]
- [README.md][]

or [browse][] the files.

## Build 

You need 

- `numpy` - some numerics
- `pillow` - PNG image creation 
- `svgwrite` - SVG image creation
- `wand` - SVG rendering to PNG

for these scripts.  Do 

    pip install -r requirements.txt 
    
to ensure you have those installed. 

To generate these scripts, do

    cd vassal     && ./collect.py
    cd cyberboard && ./collect.py
    cd cyberboard && ./collectgbxext.py
    cd cyberboard && ./collectgsnext.py
    cd cyberboard && ./collectgsnexp.py
    cd latex      && ./collect.py
    cp cyberboard/gbxextract.py .
    cp cyberboard/gsnextract.py .
    cp cyberboard/gsnexport.py .
    cp latex/wgexport.py . 
    
or simply, on most modern operating systems, 

    make 
    
### Usage 

    ./gbxextract.py <GBX> [<OPTIONS>]
    ./gsnextract.py <GSN> [<OPTIONS>]
    ./gsnexport.py <GSN> [<OPTIONS>]
    ./wgexport.py <PDF> <JSON> [<OPTIONS>]
    
    
Pass `-h` or `--help` as an option for a summery of available options 

### Note on `gsnexport.py`

Converting a CyberBoard scenario to a VASSAL module may take a _long_
time.  If you are not sure anything is happening, try passing the
option `-V` (or `--verbose`) to see what is going on.  The speed of
the conversion depends a lot on the graphics used in the game box. 

CyberBoard game boxes and scenarios made with CyberBoard prior to
version 3.0 are _not_ supported.  You may have some luck opening first
the game box and then the scenario with `CBDesign.exe` and
`CBPlay.exe`, respectively, and saving anew.  Of course, this requires
an installation of CyberBoard (on Linux, use
[wine](https://winehq.org)). 

Some CyberBoard game boxes and scenarios do not define the title or
version of the game.  In that case, you may pass the options `--title`
and `--version` to set the title or version, respectively. 

If the game box file (`.gbx`) cannot directly be found, use the option
`--gamebox` to specify the game box file. 

Another common problem with Cyberboard scenarios is that they do not
specify a starting map, or that the starting map is not the actual
board.  In that case, one can write a small Python script that patches
up the VASSAL module.  For example, the file
[`misc/PortStanley.py`](misc/PortStanley.py) will patch up the
scenario [_Port
Stanley_](http://limeyyankgames.co.uk/cyberboard/port-stanley) game
box, so that the VASSAL module will show the right board.  Other
options for this patch script could be to change the piece names to
something more appropriate than the auto-generated names, add charts
and tables, set the splash image, and so on.  Please refer to the
[`vassal`](vassal) module API for more. 

If you find that the board SVGs are not rendering as expected, you may
want to run `gsnexporter` first, and turn run `gsnexport.py` as 

    ./gsnexport.py <ZIP FILE> [<OPTIONS>]
    
possibly after editing SVGs 

    ./gsnextract.py <GSN FILE> [<OPTIONS>]
    unzip MyModule.zip board_0_0,svg 
    <Edit board_0_0.svg> # E.g. using Inkscape
    zip -u board_0_0.png 
    ./gsnexport.py <ZIP FILE> [<OPTIONS>]

A good SVG editor is [Inkscape](https://inkscape.org). 

## API 

The module [`vassal`](vassal) allows one to generate a VASSAL module
programmatically, or to read in a VASSAL module and manipulate it
programmatically.  It also has features for defining a _save_
(`.vsav`) file programmatically.

The module [`cyberboard`](cyberboard) allows one to read in a
CyberBoard GameBox file and retrieve information from it - such as
piece and board images. 

## License 

This is distributed on the GPL-3 license. 

## A word about copyright 

Caveat: _I am not a lawyer_.

Note, if you use these tools to convert a CyberBoard GameBox to say a
VASSAL module, then you are creating a derived product from the
originally copyrighted product (the GameBox).   Thus, if you want to
distribute, even to friends, the generated VASSAL module, you _must_
make sure that you are licensed to do so.

If the GameBox is licensed under an 
[_Open Source_](https://opensource.org/) license, like
[Creative-Commons Attribution, Share-Alike](https://creativecommons.org/licenses/by-sa/4.0/), GPL-3,
or similar, you are explicitly permitted to distribute your derived
work. 

If, on the other hand, the license states _all rights reserved_ then
you cannot redistribute without explicit permission by the copyright
holder.

If no license is explicitly given, then the default is akin to 
_all rights reserved_. 

Note, if the converted data contains graphics or the like, it may not
be the module developer that holds the copyright to the materials.
The developer may (or may not) have obtained permission to distribute
the materials, but that does not imply that permission is given to
third party. 

However, what is copyrightable and what isn't [is not
obvious](https://boardgamegeek.com/thread/493249/).  Only _original_
and _artistic_ forms of _expression_ can be copyrighted.  Ideas, and
similar cannot.  That means that the mechanics of a game cannot be
copyrighted.  The exact graphics and phrasing of the rules _can_.
However, if you make distinctive new graphics and rephrase the rules,
it is _not_ subject the original copyright.  Note, however, that it is
not enough to change a colour or font here or there - it has to be
_original_.

Note that you are free to make your own copy, as long as you obtained
the original legally.  Copyright only matters if you plan to
_redistribute_, irrespective of whether or not you monetise the
redistribution.

    
  
[artifacts.zip]: https://gitlab.com/wargames_tex/pywargame/-/jobs/artifacts/master/download?job=dist
[vassal.py]: https://gitlab.com/wargames_tex/pywargame/-/jobs/artifacts/master/file/public/vassal.py?job=dist
[cyberboard.py]: https://gitlab.com/wargames_tex/pywargame/-/jobs/artifacts/master/file/public/cyberboard.py?job=dist
[gbxextract.py]: https://gitlab.com/wargames_tex/pywargame/-/jobs/artifacts/master/file/public/gbxextract.py?job=dist
[gsnextract.py]: https://gitlab.com/wargames_tex/pywargame/-/jobs/artifacts/master/file/public/gsnextract.py?job=dist
[gsnexport.py]: https://gitlab.com/wargames_tex/pywargame/-/jobs/artifacts/master/file/public/gsnexport.py?job=dist
[wgexport.py]: https://gitlab.com/wargames_tex/pywargame/-/jobs/artifacts/master/file/public/wgexport.py?job=dist
[requirements.txt]: https://gitlab.com/wargames_tex/pywargame/-/jobs/artifacts/master/file/public/requirements.txt?job=dist
[README.md]: https://gitlab.com/wargames_tex/pywargame/-/jobs/artifacts/master/file/public/README.md?job=dist
[browse]: https://gitlab.com/wargames_tex/pywargame/-/jobs/artifacts/master/browse/public?job=dist



