from vassal.base import key, ALT
from vassal.traits import *

def patch(buildFile,moduleData,vmod,verbose):
    game = buildFile.getGame()
    boards  = game.getMaps()
    map     = boards['Port Stanley Map']
    crt     = boards['CRT and other Tables']
    turnrec = boards['Turn Record Track']
    game.remove(map)
    game.insertBefore(map,crt)
    
    crt['launch']         = True
    crt['buttonName']     = 'CRT'
    crt['hotkey']         = key('B',ALT)
    crt['markMoved']      = 'Never'
    #crt['icon']           = '/images/chart.gif',
    turnrec['launch']     = True
    turnrec['buttonName'] = 'Turns'
    turnrec['hotkey']     = key('S',ALT)
    turnrec['markMoved']  = 'Never'
    #turnrec['icon']       = '/images/status.gif',


    pieceNames = { 
      # 
      'piece_0_00':	 'Pucara',
      # 
      'piece_0_01':	 'Skyhawk',
      # 
      'piece_0_02':	 'Mirage III',
      # 
      'piece_0_03':	 'Harrier',
      # A & B Company, 40 Commando Royal Marines, 3rd Commando Brigade Royal Marines
      'piece_1_00':	 'GB/A&B/40/3 marico',
      # C & D Company, 40 Commando Royal Marines, 3rd Commando Brigade Royal Marines
      'piece_1_01':	 'GB/C&D/30/3 marico',
      # J & K Company, 42 Commando Royal Marines, 3rd Commando Brigade Royal Marines
      'piece_1_02':	 'GB/J&K/42/3 marico',
      # L Company, 42 Commando Royal Marines, 3rd Commando Brigade Royal Marines
      'piece_1_03':	 'GB/L/42/3 marico',
      # X & Y Company, 45 Commando Royal Marines, 3rd Commando Brigade Royal Marines
      'piece_1_04':	 'GB/X&Y/45/3 marico',
      # Z Company, 45 Commando Royal Marines, 3rd Commando Brigade Royal Marines
      'piece_1_05':	 'GB/Z/45/3 marico',
      # A & B Company, 2nd Battalion Parachute Regiment, 3rd Commando Brigade Royal Marines
      'piece_1_06':	 'GB/A&B/2/3 abico',
      # C & D Company, 2nd Battalion Parachute Regiment, 3rd Commando Brigade Royal Marines
      'piece_1_07':	 'GB/C&D/2/3 abico',
      # A & B Company, 3rd Battalion Parachute Regiment, 3rd Commando Brigade Royal Marines
      'piece_1_08':	 'GB/A&B/3/3 abico',
      # C & D Company, 3rd Battalion Parachute Regiment, 3rd Commando Brigade Royal Marines
      'piece_1_09':	 'GB/C&D/3/3 abico',
      # (Special Force), D & G Squadrons, 22nd SAS Regiment, 3rd Commando Brigade Royal Marines
      'piece_1_10':	 'GB/D&G/22SAS/3 sofsq',
      # (Special Force), 3 & 6 Section Special Boat Squadron, Royal Marines, 3rd Commando Brigade Royal Marines, (No.2 section was sent to South Georgia)
      'piece_1_11':	 'GB/3&6SBS/3 sofsc',
      # 2 Medium Reconnaissansce Troops, B Squadron, the Blues & Royals, 3rd Commando Brigade Royal Marines, (Scorpion x4 , Scimitar light tank x4 , Samson recovery vehicle x1)
      'piece_1_12':	 'GB/B/3 arecsq',
      # 8 Battery? , 29 Commando Regiment Royal Artillery, 3rd Commando Brigade Royal Marines, (105mm L118 Field Gun)
      'piece_1_13':	 'GB/8/29/3 artbat',
      # ? Battery , 29 Commando Regiment Royal Artillery, 3rd Commando Brigade Royal Marines, (105mm L118 Field Gun)
      'piece_1_14':	 'GB/X/29/3 artbat',
      # ? Battery , 29 Commando Regiment Royal Artillery, 3rd Commando Brigade Royal Marines, (105mm L118 Field Gun)
      'piece_1_15':	 'GB/Y/29/3 artbat',
      # T Battery 12 Air Defence Regiment, 3rd Commando Brigade Royal Marines, (BAe Rapier SAM)
      'piece_1_16':	 'GB/1/T/12/3 aabat',
      # T Battery 12 Air Defence Regiment, 3rd Commando Brigade Royal Marines, (BAe Rapier SAM)
      'piece_1_17':	 'GB/2/T/12/3 aabat',
      # T Battery 12 Air Defence Regiment, 3rd Commando Brigade Royal Marines, (BAe Rapier SAM)
      'piece_1_18':	 'GB/3/T/12/3 aabat',
      # 3rd Commando Brigade Air Squadron Royal Marines
      'piece_1_19':	 'GB/A/3 hesq',
      # 3rd Commando Brigade Air Squadron Royal Marines
      'piece_1_20':	 'GB/B/3 hesq',
      # 3rd Commando Brigade Air Squadron Royal Marines
      'piece_1_21':	 'GB/C/3 hesq',
      # 656 AAC Squadron
      'piece_1_22':	 'GB/D/656 aacsq',
      # 656 AAC Squadron
      'piece_1_23':	 'GB/E/656 accsq',
      # (Gurkha), A & B Company, 1st Battalion, 7th Duke of Edinburgh's Own Gurkha Rifles, 5th Infantry Brigade
      'piece_1_24':	 'GB/A&B/1/7/5 mntico',
      # (Gurkha), C & D Company, 1st Battalion, 7th Duke of Edinburgh's Own Gurkha Rifles, 5th Infantry Brigade
      'piece_1_25':	 'GB/C&D/1/7/5 mntico',
      # 1 & 2 Company, 1st Battalion Welsh Guards, 5th Infantry Brigade
      'piece_1_26':	 'GB/1&2/1/5 ico',
      # 3 & Prince of Wales Company, 1st Battalion Welsh Guards, 5th Infantry Brigade
      'piece_1_27':	 'GB/3&PW/1/5 ico',
      # Left Flank & F? Company, 2nd Battalion Scots Guards, 5th Infantry Brigade
      'piece_1_28':	 'GB/LF&F/2/5 ico',
      # G & Right Flank Company, 2nd Battalion Scots Guards, 5th Infantry Brigade
      'piece_1_29':	 'GB/G&RF/2/5 ico',
      # 97 Battery, 4th Regiment Royal Artillery, 5th Infantry Brigade, (105mm L118 Field Gun)
      'piece_1_30':	 'GB/97/4/5 artbat',
      # 63 Squadron RAF Regiment? or, 21 Air Defence Battery, Royal Artillery?, 5th Infantry Brigade, (Blowpipe SAM?)
      'piece_1_31':	 'GB/63RAW/21/5 aasq',
      # 33 Engineer Regiment, 5th Infantry Brigade
      'piece_1_32':	 'GB/33/5 engregt',
      # HMS Glamorgan
      'piece_2_00':	 'GB/HMS Glamorgan',
      # HMS Brilliant, HMS Coventry
      'piece_2_01':	 'GB/HMS Brilliant and HMS Coventry',
      # HMS Arrow, HMS Antelope
      'piece_2_02':	 'GB/HMS Arrow and HMS Antelope',
      # HMS Plymouth, HMS Yarmouth
      'piece_2_03':	 'GB/HMS Plymouth and HMS Yarmouth',
      # RFA Intrepid
      'piece_2_04':	 'GB/RFA Intrepid',
      # HMS Fearless
      'piece_2_05':	 'GB/HMS Fearless',
      # RFA Sir Lancelot, RFA Sir Garaint
      'piece_2_06':	 'GB/RFA Sir Lancelot and RFA Sir Garaint',
      # RFA Sir Galahad, RFA Sir Tristam
      'piece_2_07':	 'GB/RFA Sir Galahad and RFA Sir Tristam',
      # RFA Sir Bedivere, RFA Sir Percivale
      'piece_2_08':	 'GB/RFA Sir Bedivere and RFA Sir Percivale',
      # 
      'piece_2_09':	 'GB/A/Task force',
      # 
      'piece_2_10':	 'GB/B/Task force',
      # 
      'piece_2_11':	 'GB/C/Task force',
      # HMS Antrim
      'piece_2_12':	 'GB/HMS Antrim',
      # HMS Broadsword
      'piece_2_13':	 'GB/HMS Broadsword',
      # HMS Ardent
      'piece_2_14':	 'GB/HMS Ardent',
      # 
      'piece_3_00':	 'GB/Sea Harrier 6',
      # 
      'piece_3_01':	 'GB/Sea Harrier 4',
      # 
      'piece_3_02':	 'GB/Sea Harrier 2 (1)',
      # 
      'piece_3_03':	 'GB/Sea Harrier 2 (2)',
      # 
      'piece_3_04':	 'GB/Sea Harrier 2 (3)',
      # 1st Battalion, 3rd Regiment, 10th Motorised Infantry Brigade
      'piece_4_00':	 'AG/1/3/10 mibtn',
      # 2nd Battalion, 3rd Regiment, 10th Motorised Infantry Brigade
      'piece_4_01':	 'AG/2/3/10 mibtn',
      # 3rd Battalion, 3rd Regiment, 10th Motorised Infantry Brigade
      'piece_4_02':	 'AG/3/3/10 mibtn',
      # 1st Battalion, 4th Regiment, 3rd Mechanised Infantry Brigade
      'piece_4_03':	 'AG/1/4/10 aibtn',
      # 2nd Battalion, 4th Regiment, 3rd Mechanised Infantry Brigade
      'piece_4_04':	 'AG/2/4/10 aibtn',
      # 3rd Battalion, 4th Regiment, 3rd Mechanised Infantry Brigade
      'piece_4_05':	 'AG/3/4/10 aibtn',
      # 1st Battalion, 6th Regiment, 10th Motorised Infantry Brigade
      'piece_4_06':	 'AG/1/6/10 mibtn',
      # 2nd Battalion, 6th Regiment, 10th Motorised Infantry Brigade
      'piece_4_07':	 'AG/2/6/10 mibtn',
      # 3rd Battalion, 6th Regiment, 10th Motorised Infantry Brigade
      'piece_4_08':	 'AG/3/6/10 mibtn',
      # 1st Battalion, 7th Regiment, 10th Motorised Infantry Brigade
      'piece_4_09':	 'AG/1/7/10 mibtn',
      # 2nd Battalion, 7th Regiment, 10th Motorised Infantry Brigade
      'piece_4_10':	 'AG/2/7/10 mibtn',
      # 3rd Battalion, 7th Regiment, 10th Motorised Infantry Brigade
      'piece_4_11':	 'AG/3/7/10 mibtn',
      # 1st Battalion, 12th Regiment, 3rd Mechanised Infantry Brigade
      'piece_4_12':	 'AG/1/12/3 mibtn',
      # 1 Company, 2nd Battalion, 12th Regiment, 3rd Mechanised Infantry Brigade
      'piece_4_13':	 'AG/1/2/12/3 mico',
      # 2 Company, 2nd Battalion, 12th Regiment, 3rd Mechanised Infantry Brigade
      'piece_4_14':	 'AG/2/2/12/3 mico',
      # 1 Company, 3rd Battalion, 12th Regiment, 3rd Mechanised Infantry Brigade
      'piece_4_15':	 'AG/1/3/12/3 mico',
      # 2 Company, 3rd Battalion, 12th Regiment, 3rd Mechanised Infantry Brigade
      'piece_4_16':	 'AG/2/3/12/3 mico',
      # 1 Platoon, 3 Company, 3rd Battalion, 12th Regiment, 3rd Mechanised Infantry Brigade
      'piece_4_17':	 'AG/1/3/3/12/3 mipl',
      # 2 Platoon, 3 Company, 3rd Battalion, 12th Regiment, 3rd Mechanised Infantry Brigade
      'piece_4_18':	 'AG/2/3/3/12/3 mipl',
      # 1st Battalion, Independent 25th Regiment
      'piece_4_19':	 'AG/1/25 ibtn',
      # 2nd Battalion, Independent 25th Regiment
      'piece_4_20':	 'AG/2/25 ibtn',
      # 3rd Battalion, Independent 25th Regiment
      'piece_4_21':	 'AG/3/25 ibtn',
      # 3rd Marine Infantry Battalion
      'piece_4_22':	 'AG/3 maribat',
      # 1 Company, 5th Marine Infantry Battalion
      'piece_4_23':	 'AG/1/5 marico',
      # 1 Company, 1st Battalion, 1st Airborne Regiment
      'piece_4_24':	 'AG/1/1/1 abico',
      # 601st Ranger Company
      'piece_4_25':	 'AG/601 sofco',
      # 602nd Ranger Company
      'piece_4_26':	 'AG/602 sofco',
      # 10th Armoured Cavalry Reconaissance Squadron, (Panhard AML90 Armoured Car)
      'piece_4_27':	 'AG/10 arecsq',
      # 3rd Artillery Btn, (105mm OTO-Melara Pack Howitzer Gun, 155mm CITEFA Model 77 Gun)
      'piece_4_28':	 'AG/1/3 artbtn',
      # 3rd Artillery Btn, (105mm OTO-Melara Pack Howitzer Gun, 155mm CITEFA Model 77 Gun)
      'piece_4_29':	 'AG/2/3 artbtn',
      # 3rd Artillery Btn, (105mm OTO-Melara Pack Howitzer Gun, 155mm CITEFA Model 77 Gun)
      'piece_4_30':	 'AG/3/3 artbtn',
      # 1st Marine Artillery Battalion
      'piece_4_31':	 'AG/1/1 artbtn',
      # 1st Marine Artillery Battalion
      'piece_4_32':	 'AG/2/1 artbtn',
      # 11th Artillery Group
      'piece_4_33':	 'AG/11 artgrb',
      # 601st Anti-Aircraft Battalion
      'piece_4_34':	 'AG/1/601 aabtn',
      # 601st Anti-Aircraft Battalion
      'piece_4_35':	 'AG/2/601 aabtn',
      # Marine Anti-Aircraft Battalion
      'piece_4_36':	 'AG/1/mar aabtn',
      # Marine Anti-Aircraft Battalion
      'piece_4_37':	 'AG/2/mar aabtn',
      # M-113 APC? /, LVTP-7s  Landing Vehicle Tracked Personnel?
      'piece_4_38':	 'AG/1 lvtp',
      # M-113 APC? /, LVTP-7s  Landing Vehicle Tracked Personnel?
      'piece_4_39':	 'AG/2 lvtp',
      # Joint Force HQ, Major General Mario Menendez
      'piece_4_40':	 'AG/jf hq',
      # 10th Motorised Infantry Brigade, Major General Oscar Joffre
      'piece_4_41':	 'AG/10 mihq',
      # Exocet missile unit
      'piece_4_42':	 'AG/exocet',
      # 
      'piece_4_43':	 'AG/dummy 1',
      # 
      'piece_4_44':	 'AG/dummy 2',
      # 
      'piece_4_45':	 'AG/dummy 3',
      # 
      'piece_4_46':	 'AG/dummy 4',
      # 
      'piece_4_47':	 'AG/dummy 5',
      # 
      'piece_4_48':	 'AG/dummy 6',
      # 
      'piece_4_49':	 'AG/A he',
      # 
      'piece_4_50':	 'AG/B he',
      # 
      'piece_5_00':	 'AG/Pucara 4 (1)',
      # 
      'piece_5_01':	 'AG/Pucara 4 (2)',
      # 
      'piece_5_02':	 'AG/Pucara 2 (1)',
      # 
      'piece_5_03':	 'AG/Pucara 2 (3)',
      # 
      'piece_5_04':	 'AG/Pucara 2 (4)',
      # 
      'piece_5_05':	 'AG/Pucara 2 (5)',
      # 
      'piece_5_06':	 'AG/Mirage III 6 (1)',
      # 
      'piece_5_07':	 'AG/Mirage III 6 (2)',
      # 
      'piece_5_08':	 'AG/Mirage III 4 (1)',
      # 
      'piece_5_09':	 'AG/Mirage III 4 (2)',
      # 
      'piece_5_10':	 'AG/Mirage III 2 (1)',
      # 
      'piece_5_11':	 'AG/Mirage III 2 (2)',
      # 
      'piece_5_12':	 'AG/Skyhawk 6',
      # 
      'piece_5_13':	 'AG/Skyhawk 4 (1)',
      # 
      'piece_5_14':	 'AG/Skyhawk 4 (2)',
      # 
      'piece_5_15':	 'AG/Skyhawk 2 (1)',
      # 
      'piece_5_16':	 'AG/Skyhawk 2 (2)',
      # 
      'piece_5_17':	 'AG/airstrip (1)',
      # 
      'piece_5_18':	 'AG/airstrip (2)',
      # 
      'piece_5_19':	 'AG/airstrip (3)',
      # 
      'piece_5_20':	 'AG/airstrip (4)',
      # 
      'piece_5_21':	 'AG/airstrip (5)',
      # 
      'piece_5_22':	 'AG/airstrip (6)',
      # 
      'piece_6_00':	 'AG/ico 2-2-12/1',
      # 
      'piece_6_01':	 'AG/ico 2-2-12/2',
      # 
      'piece_6_02':	 'AG/ico 2-2-12/3',
      # 
      'piece_6_03':	 'AG/ico 2-2-12/4',
      # 
      'piece_6_04':	 'AG/ico 2-2-12/5',
      # 
      'piece_6_05':	 'AG/ico 2-2-12/6',
      # 
      'piece_6_06':	 'AG/marico/2-3-14',
      # 
      'piece_6_07':	 'AG/ico/1-3-14/1',
      # 
      'piece_6_08':	 'AG/ico/1-3-14/2',
      # 
      'piece_6_09':	 'AG/ico/1-3-12/1',
      # 
      'piece_6_10':	 'AG/ico/1-3-14/2',
      # 
      'piece_6_11':	 'AG/ico/1-3-14/3',
      # 
      'piece_6_12':	 'AG/ico/1-3-14/4',
      # 
      'piece_6_13':	 'AG/ico/1-3-14/5',
      # 
      'piece_6_14':	 'AG/ico/1-3-14/6',
      # 
      'piece_6_15':	 'AG/ipl/0-3-14/1',
      # 
      'piece_6_16':	 'AG/ipl/0-3-14/2',
      # 
      'piece_6_17':	 'AG/ipl/0-3-14/3',
      # 
      'piece_7_00':	 'GB/Beach 1',
      # 
      'piece_7_01':	 'GB/Beach 2',
      # 
      'piece_7_02':	 'GB/Beach 3',
      # 
      'piece_7_03':	 'GB/Beach 4',
      # 
      'piece_7_04':	 'GB/Beach 5',
      # 
      'piece_7_05':	 'GB/Beach 6',
      # 
      'piece_7_06':	 'GB/Beach 7',
      # 
      'piece_7_07':	 'GB/Beach 8',
      # 
      'piece_7_08':	 'GB/Beach 9',
      # 
      'piece_7_09':	 'GB/Beach 10',
      # 
      'piece_7_10':	 'GB/Beach 11',
    }

    markNames = {
      # 
      'mark_00_00':	 'Dice 1',
      # 
      'mark_00_01':	 'Dice 2',
      # 
      'mark_00_02':	 'Dice 3',
      # 
      'mark_00_03':	 'Dice 4',
      # 
      'mark_00_04':	 'Dice 5',
      # 
      'mark_00_05':	 'Dice 6',
      # 
      'mark_01_00':	 'Red attack SW',
      # 
      'mark_01_01':	 'Red attack NW',
      # 
      'mark_01_02':	 'Red attack NE',
      # 
      'mark_01_03':	 'Red attack SE',
      # 
      'mark_01_04':	 'Red attack S',
      # 
      'mark_01_05':	 'Red attack N',
      # 
      'mark_01_06':	 'Blue attack SW',
      #                                
      'mark_01_07':	 'Blue attack NW',
      #                                
      'mark_01_08':	 'Blue attack NE',
      #                                
      'mark_01_09':	 'Blue attack SE',
      #                                
      'mark_01_10':	 'Blue attack S',
      #                                
      'mark_01_11':	 'Blue attack N',
      # 
      'mark_02_00':	 'Turn marker',
      # 
      'mark_02_01':	 'Turn marker small',
      # British Air Reconnaissance, unit assigned to ground attack / support mission
      'mark_03_00':	 'GB air reconnaissance',
      # Argentine Air / Artillery attack
      'mark_03_01':	 'AR air or artillery attack',
      # British Air / Naval / Artillery attack
      'mark_03_02':	 'GB air, naval, or artillery attack',
      # +1 defender strength point, British Artillery indirect support and / or Air or Naval support
      'mark_03_03':	 '+1 DF from GB support',
      # +2 defender strength point, British Artillery indirect support and / or Air or Naval support
      'mark_03_04':	 '+2 DF from GB support',
      # +3 defender strength point, British Artillery indirect support and / or Air or Naval support
      'mark_03_05':	 '+3 DF from GB support',
      # +4 defender strength point, British Artillery indirect support and / or Air or Naval support
      'mark_03_06':	 '+4 DF from GB support',
      # 1 step lost, British Artillery barrage , Naval or Air attack result 
      'mark_03_07':	 '1 step loss from GB support',
      # 2 steps lost, British Artillery barrage , Naval or Air attack result 
      'mark_03_08':	 '2 step loss from GB support',
      # 3 steps lost, British Artillery barrage , Naval or Air attack result 
      'mark_03_09':	 '3 step loss from GB support',
      # 4 steps lost, British Artillery barrage , Naval or Air attack result 
      'mark_03_10':	 '4 step loss from GB support',
      # +1 defender strength point, Argentine Artillery indirect support and / or Air support
      'mark_03_11':	 '+1 DF from AG support',
      # +2 defender strength point, Argentine Artillery indirect support and / or Air support
      'mark_03_12':	 '+2 DF from AG support',
      # +3 defender strength point, Argentine Artillery indirect support and / or Air support
      'mark_03_13':	 '+3 DF from AG support',
      # +4 defender strength point, Argentine Artillery indirect support and / or Air support
      'mark_03_14':	 '+4 DF from AG support',
      # 1 step lost, Argentine Artillery barrage or Air attack result 
      'mark_03_15':	 '1 step loss from AG support',
      # 2 steps lost, Argentine Artillery barrage or Air attack result 
      'mark_03_16':	 '2 step loss from AG support',
      # 3 steps lost, Argentine Artillery barrage or Air attack result 
      'mark_03_17':	 '3 step loss from AG support',
      # 4 steps lost, Argentine Artillery barrage or Air attack result 
      'mark_03_18':	 '4 step loss from AG support',
      # 
      'mark_04_00':	 'Magenta 0',
      # 
      'mark_04_01':	 'Magenta 1',
      # 
      'mark_04_02':	 'Magenta 2',
      # 
      'mark_04_03':	 'Magenta 3',
      # 
      'mark_04_04':	 'Magenta 4',
      # 
      'mark_04_05':	 'Magenta 5',
      # 
      'mark_04_06':	 'Magenta 6',
      # 
      'mark_04_07':	 'Magenta 7',
      # 
      'mark_04_08':	 'Magenta 8',
      # 
      'mark_04_09':	 'Magenta 9',
      # 
      'mark_04_10':	 'Cyan 0',
      #                            
      'mark_04_11':	 'Cyan 1',
      #                            
      'mark_04_12':	 'Cyan 2',
      #                            
      'mark_04_13':	 'Cyan 3',
      #                            
      'mark_04_14':	 'Cyan 4',
      #                            
      'mark_04_15':	 'Cyan 5',
      #                            
      'mark_04_16':	 'Cyan 6',
      #                            
      'mark_04_17':	 'Cyan 7',
      #                            
      'mark_04_18':	 'Cyan 8',
      #                            
      'mark_04_19':	 'Cyan 9',
      # 
      'mark_04_20':	 'Green 0',
      #                            
      'mark_04_21':	 'Green 1',
      #                            
      'mark_04_22':	 'Green 2',
      #                            
      'mark_04_23':	 'Green 3',
      #                            
      'mark_04_24':	 'Green 4',
      #                            
      'mark_04_25':	 'Green 5',
      #                            
      'mark_04_26':	 'Green 6',
      #                            
      'mark_04_27':	 'Green 7',
      #                            
      'mark_04_28':	 'Green 8',
      #                            
      'mark_04_29':	 'Green 9',
      # 
      'mark_05_00':	 'A',
      # 
      'mark_05_01':	 'B',
      # 
      'mark_05_02':	 'C',
      # 
      'mark_05_03':	 'D',
      # 
      'mark_05_04':	 'E',
      # 
      'mark_05_05':	 'F',
      # 
      'mark_05_06':	 'G',
      # 
      'mark_05_07':	 'H',
      # 
      'mark_05_08':	 'I',
      # 
      'mark_05_09':	 'J',
      # 
      'mark_05_10':	 'K',
      # 
      'mark_05_11':	 'L',
      # 
      'mark_05_12':	 'M',
      # 
      'mark_05_13':	 'N',
      # 
      'mark_05_14':	 'O',
      # 
      'mark_05_15':	 'P',
      # 
      'mark_05_16':	 'Q',
      # 
      'mark_05_17':	 'R',
      # 
      'mark_05_18':	 'S',
      # 
      'mark_05_19':	 'T',
      # 
      'mark_05_20':	 'U',
      # 
      'mark_05_21':	 'V',
      # 
      'mark_05_22':	 'X',
      # 
      'mark_05_23':	 'Y',
      # 
      'mark_05_24':	 'Z',
      # Each engaged unit which fails a morale check loses 1 step and must retreat 1 hex;, Each engaged unit which passes a morale check must retreat 1 hex
      'mark_06_00':	 'Step loss',
      # Each engaged unit which fails a morale check loses 1 step and must retreat 1 hex;, Each engaged unit which passes a morale check must retreat 1 hex, Add +1 to the die roll when making morale checks
      'mark_06_01':	 'Step loss, +1 morale check',
      # Each engaged unit which fails a morale check loses 1 step and must retreat 1 hex;, Each engaged unit which passes a morale check must retreat 1 hex, Add +2 to the die roll when making morale checks
      'mark_06_02':	 'Step loss, +2 morale check',
      # No effect
      'mark_06_03':	 'No effect',
      # Each engaged unit which fails a morale check must retreat 1 hex;, Each engaged unit which passes a morale check  in unaffected
      'mark_06_04':	 'Retreat',
      # Each engaged unit which fails a morale check must retreat 1 hex;, Each engaged unit which passes a morale check  in unaffected, Add +1 to the die roll when making morale check
      'mark_06_05':	 'Retreat, +1 morale check',
      # Each engaged unit which fails a morale check must retreat 1 hex;, Each engaged unit which passes a morale check  in unaffected, Add +2 to the die roll when making morale check
      'mark_06_06':	 'Retreat, +2 morale check',
      # Each engaged unit which fails a morale check is eliminated;, Each engaged unit which passes a morale check must lose 1 step and retreat 1 hex 
      'mark_06_07':	 'Eliminated',
      # Each engaged unit which fails a morale check is eliminated;, Each engaged unit which passes a morale check must lose 1 step and retreat 1 hex , Add +1 to the die roll when making morale check
      'mark_06_08':	 'Eliminated, +1 morale check',
      # Each engaged unit which fails a morale check is eliminated;, Each engaged unit which passes a morale check must lose 1 step and retreat 1 hex , Add +2 to the die roll when making morale check
      'mark_06_09':	 'Eliminated, +2 morale check',
      # Demoralized, Effectivenes Rating -1
      'mark_06_10':	 'Demoralized, -1 effectiveness',
      # 
      'mark_07_00':	 'Bridge, NE',
      # 
      'mark_07_01':	 'Bridge, NW',
      # 
      'mark_07_02':	 'Bridge N',
      # 
      'mark_07_03':	 'Broken bridge, NW',
      # 
      'mark_07_04':	 'Broken bridge, NE',
      # 
      'mark_07_05':	 'Broken bridge, N',
      # 
      'mark_07_06':	 'Bridge work, NE',
      # 
      'mark_07_07':	 'Bridge work, NW',
      # 
      'mark_07_08':	 'Bridge work, N',
      # 
      'mark_07_09':	 'Road, N',
      # 
      'mark_07_10':	 'Road, NE',
      # 
      'mark_07_11':	 'Road, NW',
      # 
      'mark_07_12':	 'Road work, N',
      # 
      'mark_07_13':	 'Road work, NE',
      # 
      'mark_07_14':	 'Road work, NW',
      # 
      'mark_07_15':	 'At work, red',
      # 
      'mark_07_16':	 'At work, orange',
      # 
      'mark_07_17':	 'At work, blue',
      # 
      'mark_07_18':	 'At work, cyan',
      # 
      'mark_07_19':	 'Hexagon, red 2',
      # 
      'mark_07_20':	 'Hexagon, red 1',
      # 
      'mark_07_21':	 'Hexagon, blue 2',
      # 
      'mark_07_22':	 'Hexagon, blue 1',
      # 
      'mark_08_00':	 'AG supply, 6',
      # 
      'mark_08_01':	 'AG supply, 5',
      # 
      'mark_08_02':	 'AG supply, 4',
      # 
      'mark_08_03':	 'AG supply, 3',
      # 
      'mark_08_04':	 'AG supply, 2',
      # 
      'mark_08_05':	 'AG supply, 1',
      # 
      'mark_08_06':	 'GB out of supply',
      # 
      'mark_08_07':	 'AG supply, 2',  
      #                                   
      'mark_08_08':	 'AG supply, 1',  
      #                                   
      'mark_08_09':	 'AG out of supply',  
      #                                   
      'mark_08_10':	 'GB Supply, 8',  
      #                                   
      'mark_08_11':	 'GB Supply, 7',  
      #                                   
      'mark_08_12':	 'GB Supply, 6',  
      #                                   
      'mark_08_13':	 'GB supply, 5',
      # 
      'mark_08_14':	 'GB supply, 4',
      # 
      'mark_08_15':	 'GB supply, 3',
      # 
      'mark_08_16':	 'GB supply, 2',
      # 
      'mark_08_17':	 'GB supply, 1',
      # 
      'mark_08_18':	 'GB out of supply',
      # British Controled Airfield
      'mark_09_00':	 'GB airfield',
      # Argentine Controled Airfield
      'mark_09_01':	 'AG airfield',
      # Destroyed Airfield, (out of service)
      'mark_09_02':      'Destroyed airfield'
    }

    pieces = game.getPieces(asdict=False)
    for piece in pieces:
        traits = piece.getTraits()
        basic  = traits.pop()
        name   = basic['name']
        newn   = pieceNames.get(name, markNames.get(name,None))
        if newn is None:
            continue

        newn = newn.replace(',','\\,').replace('/','_')
        basic['name'] = newn

        if newn.startswith('GB_'):
            traits.append(MarkTrait(name='Faction',value='British'))
        elif newn.startswith('AG_'):
            traits.append(MarkTrait(name='Faction',value='Argentine'))
        
        piece.setTraits(*traits,basic)
        piece['entryName'] = newn
        
#
# EOF
#
