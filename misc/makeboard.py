from json import load

d = load(open('info.json','r'))

#for k,b in d['gamebox']['boards'].items():
#    print(k,b['name'])
    

mainMap  = d['gamebox']['boards']['9']
cells    = mainMap['cells']
colorMap = {(0,109,204): 'sea',
            (0,109,206): 'sea',
            (85,176,255): 'shallow sea' }
tileMap = {'tile_0561.png': 'rough',
           'tile_0586.png': 'clear' }

for row in cells['list']:
    for cell in row:
        color = cell.get('color','none')
        tile  = cell.get('tile',None)
        terrain = None
        if color != 'none':
            from re import match
            m       = match(r'rgb\(([0-9]+),([0-9]+),([0-9]+)\)',color)
            terrain = colorMap.get((int(m[1]),int(m[2]),int(m[3])),None)
        elif tile is not None:
            terrain = tileMap.get(tile,tile)

        # if (color != 'none' or tile is not None) and terrain is None:
        #     # print(f'{color} {tile} -> {terrain}')

        if terrain is None:
            continue

        row = cell['row']
        col = cell['column']

        print(rf'  \hex[terrain={terrain}](c={col},r={row});')
            
            
