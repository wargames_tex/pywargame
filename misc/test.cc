// This is a part of the Microsoft Foundation Classes C++ library.
// Copyright (C) Microsoft Corporation
// All rights reserved.
//
// This source code is only intended as a supplement to the
// Microsoft Foundation Classes Reference and related
// electronic documentation provided with the library.
// See these sources for detailed information regarding the
// Microsoft Foundation Classes product.

#include <cassert>
#include <iostream>
#include <cstdio>
#include <cstring>
#include <string>


// #define new DEBUG_NEW

////////////////////////////////////////////////////////////////////////////
// Serialize member functions for low level classes put here
// for code swapping improvements

// minimum buffer size
enum { nBufSizeMin = 128 };
// default amount to grow m_pLoadArray upon insert
enum { nGrowSize = 64 };
// default size of hash table in m_pStoreMap when storing
enum { nHashSize = 137 };
// default size to grow collision blocks when storing
enum { nBlockSize = 16 };


using ULONGLONG=unsigned long;
using DWORD=unsigned int;
using WORD=unsigned short;
using BYTE=unsigned char;
using BOOL=bool;
using _TUCHAR=unsigned char;
using UINT=unsigned int;
using LONG=long;
using ULONG=unsigned long;
#define TRY try
#define CATCH catch
BOOL TRUE = true;
BOOL FALSE = false;
  
class CArchive
{
public:
  // Flag values
  enum Mode { store = 0, load = 1, bNoFlushOnDelete = 2, bNoByteSwap = 4 };

  CArchive(FILE* pFile, UINT nMode, int nBufSize = 4096, void* lpBuf = NULL);
  ~CArchive();

  // Attributes
  BOOL IsLoading() const { return (m_nMode & CArchive::load) != 0; }
  BOOL IsStoring() const { return (m_nMode & CArchive::load) == 0; }
  BOOL IsByteSwapping() const { return false; }
  BOOL IsBufferEmpty() const { return m_lpBufCur == m_lpBufMax; }
  
  FILE* GetFile() const;

  // Operations
  UINT Read(void* lpBuf, UINT nMax);
  void Write(const void* lpBuf, UINT nMax);
  void Flush();
  void Close();
  void Abort();   // close and shutdown without exceptions

public:
  // insertion operations
  CArchive& operator<<(BYTE by) {
    if (m_lpBufCur + sizeof(BYTE) > m_lpBufMax) Flush();
    *(BYTE*)m_lpBufCur = by;
    m_lpBufCur += sizeof(BYTE);
    return *this;
  }
  CArchive& operator<<(WORD w) {
    if (m_lpBufCur + sizeof(WORD) > m_lpBufMax) Flush();
    *(WORD*)m_lpBufCur = w;
    m_lpBufCur += sizeof(WORD);
    return *this;
  }
  CArchive& operator<<(LONG l) {
    if (m_lpBufCur + sizeof(LONG) > m_lpBufMax) Flush();
    *(LONG*)m_lpBufCur = l;
    m_lpBufCur += sizeof(LONG);
    return *this;
  }
  CArchive& operator<<(DWORD dw) {
    if (m_lpBufCur + sizeof(DWORD) > m_lpBufMax) Flush();
    *(DWORD*)m_lpBufCur = dw;
    m_lpBufCur += sizeof(DWORD);
    return *this;
  }
  CArchive& operator<<(ULONGLONG dwdw) {
    if (m_lpBufCur + sizeof(ULONGLONG) > m_lpBufMax) Flush();
    *(ULONGLONG*)m_lpBufCur = dwdw;
    m_lpBufCur += sizeof(ULONGLONG);
    return *this;
  }
  
  CArchive& operator<<(int i) { return CArchive::operator<<((LONG)i); }
  CArchive& operator<<(short w) { return CArchive::operator<<((WORD)w); }
  CArchive& operator<<(char ch) { return CArchive::operator<<((BYTE)ch); }
  CArchive& operator<<(bool b) { return CArchive::operator<<((BYTE)b); }

  // extraction operations
  CArchive& operator>>(BYTE& by) {
    if (m_lpBufCur + sizeof(BYTE) > m_lpBufMax)
      FillBuffer(UINT(sizeof(BYTE) - (m_lpBufMax - m_lpBufCur)));
    by = *(BYTE*)m_lpBufCur;
    m_lpBufCur += sizeof(BYTE);
    return *this;
  }
  CArchive& operator>>(WORD& w){
    if (m_lpBufCur + sizeof(WORD) > m_lpBufMax)
      FillBuffer(UINT(sizeof(WORD) - (m_lpBufMax - m_lpBufCur)));
    w = *(WORD*)m_lpBufCur;
    m_lpBufCur += sizeof(WORD);
    return *this;
  }
  CArchive& operator>>(DWORD& dw){
    if (m_lpBufCur + sizeof(DWORD) > m_lpBufMax)
      FillBuffer(UINT(sizeof(DWORD) - (m_lpBufMax - m_lpBufCur)));
    dw = *(DWORD*)m_lpBufCur;
    m_lpBufCur += sizeof(DWORD);
    return *this;
  }
  CArchive& operator>>(LONG& l){
    if (m_lpBufCur + sizeof(LONG) > m_lpBufMax)
      FillBuffer(UINT(sizeof(LONG) - (m_lpBufMax - m_lpBufCur)));
    l = *(LONG*)m_lpBufCur;
    m_lpBufCur += sizeof(LONG);
    return *this;
  }
  CArchive& operator>>(ULONGLONG& dwdw){
    if (m_lpBufCur + sizeof(ULONGLONG) > m_lpBufMax)
      FillBuffer(UINT(sizeof(ULONGLONG) - (m_lpBufMax - m_lpBufCur)));
    dwdw = *(ULONGLONG*)m_lpBufCur;
    m_lpBufCur += sizeof(ULONGLONG);
    return *this;
  }
  
  CArchive& operator>>(int& i) { return CArchive::operator>>((LONG&)i); }
  CArchive& operator>>(short& w) { return CArchive::operator>>((WORD&)w); }
  CArchive& operator>>(char& ch) { return CArchive::operator>>((BYTE&)ch); }
  CArchive& operator>>(bool& b)  { return CArchive::operator>>((BYTE&)b); }
public:
  BOOL m_bDirectBuffer;   // TRUE if m_pFile supports direct buffering
  BOOL m_bBlocking;  // TRUE if m_pFile can block for unbounded periods of time
  void FillBuffer(UINT nBytesNeeded);

  // special functions for reading and writing (16-bit compatible) counts
  ULONGLONG ReadCount();
  void WriteCount(ULONGLONG dwCount);

protected:
  // archive objects cannot be copied or assigned
  BOOL   m_nMode;
  BOOL   m_bUserBuf;
  int    m_nBufSize;
  FILE*  m_pFile;
  BYTE*  m_lpBufCur;
  BYTE*  m_lpBufMax;
  BYTE*  m_lpBufStart;

  // advanced parameters (controls performance with large archives)
  UINT m_nGrowSize;
  UINT m_nHashSize;
};

unsigned long AfxReadStringLength(CArchive& ar, int& nCharSize)
{
  ULONGLONG qwLength;
  DWORD dwLength;
  WORD wLength;
  BYTE bLength;

  nCharSize = sizeof(char);
  
  // First, try to read a one-byte length
  ar >> bLength;
  if (bLength < 0xff)
    return bLength;

  // Try a two-byte length
  ar >> wLength;
  if (wLength == 0xfffe) {
    // Unicode string.  Start over at 1-byte length
    nCharSize = sizeof(wchar_t);

    ar >> bLength;
    if (bLength < 0xff)
      return bLength;

    // Two-byte length
    ar>>wLength;
    // Fall through to continue on same branch as ANSI string
  }
  if (wLength < 0xffff)
    return wLength;

  // 4-byte length
  ar >> dwLength;
  if (dwLength < 0xffffffff)
    return dwLength;

  // 8-byte length
  ar >> qwLength;
  return qwLength;
}

void AfxWriteStringLength(CArchive& ar, unsigned long nLength, BOOL bUnicode)
{
  if (bUnicode) {
    // Tag Unicode strings
    ar << (BYTE)0xff;
    ar << (WORD)0xfffe;
  }

  if (nLength < 255) 
    ar << (BYTE)nLength;
  else if (nLength < 0xfffe) {
    ar << (BYTE)0xff;
    ar << (WORD)nLength;
  }
  else if (nLength < 0xffffffff) {
    ar << (BYTE)0xff;
    ar << (WORD)0xffff;
    ar << (DWORD)nLength;
  }
  else {
    ar << (BYTE)0xff;
    ar << (WORD)0xffff;
    ar << (DWORD)0xffffffff;
    ar << (ULONGLONG)nLength;
  }
}

////////////////////////////////////////////////////////////////////////////
// Archive object input/output


CArchive::CArchive(FILE* pFile, UINT nMode, int nBufSize, void* lpBuf) 
{
  // initialize members not dependent on allocated buffer
  m_nMode         = nMode;
  m_pFile         = pFile;
  if (IsStoring())
    m_nGrowSize = nBlockSize;
  else
    m_nGrowSize = nGrowSize;
  m_nHashSize = nHashSize;

  // initialize the buffer.  minimum size is 128
  m_lpBufStart    = (BYTE*)lpBuf;
  m_bUserBuf      = true;
  m_bDirectBuffer = false;
  m_bBlocking     = false;

  if (nBufSize < nBufSizeMin) {
    // force use of private buffer of minimum size
    m_nBufSize   = nBufSizeMin;
    m_lpBufStart = NULL;
  }
  else
    m_nBufSize = nBufSize;

  nBufSize = m_nBufSize;
  if (m_lpBufStart == NULL) {
    // check for CFile providing buffering support
    m_bDirectBuffer = FALSE;
    if (!m_bDirectBuffer) {
      // no support for direct buffering, allocate new buffer
      m_lpBufStart = new BYTE[m_nBufSize];
      m_bUserBuf   = FALSE;
    }
    else {
      // CFile* supports direct buffering!
      nBufSize = 0;   // will trigger initial FillBuffer
    }
  }
  if (!m_bDirectBuffer)	{
    assert(m_lpBufStart != NULL);
  }
  m_lpBufMax = m_lpBufStart + nBufSize;
  m_lpBufCur = (IsLoading()) ? m_lpBufMax : m_lpBufStart;
}

CArchive::~CArchive()
{
  // Close makes m_pFile NULL. If it is not NULL, we must Close the CArchive
  if (m_pFile != NULL && !(m_nMode & bNoFlushOnDelete))
    Close();
  Abort();    // abort completely shuts down the archive
}

void CArchive::Abort()
{
  // disconnect from the file
  m_pFile = NULL;

  if (!m_bUserBuf) {
    assert(!m_bDirectBuffer);
    delete[] m_lpBufStart;
    m_lpBufStart = NULL;
    m_lpBufCur   = NULL;
  }
}

void CArchive::Close()
{
  Flush();
  m_pFile = NULL;
}

UINT CArchive::Read(void* lpBuf, UINT nMax)
{
  if (nMax == 0)
    return 0;

  assert(lpBuf != NULL);
  
  if (lpBuf == NULL)
    return 0;

  if(!IsLoading())
    return 0;

  std::cout << "We need " << nMax << std::endl;
  // try to fill from buffer first
  UINT nMaxTemp =  nMax;
  UINT nTemp    =  std::min(nMaxTemp, UINT(m_lpBufMax - m_lpBufCur));
  // Checked::memcpy_s(lpBuf, nMaxTemp, m_lpBufCur, nTemp);
  std::cout << "Take " << nTemp << " from buffer" << std::endl;
  memcpy(lpBuf, m_lpBufCur, nTemp);
  m_lpBufCur    += nTemp;
  lpBuf         =  (BYTE*)lpBuf + nTemp;
  nMaxTemp      -= nTemp;

  if (nMaxTemp != 0) {
    std::cout << "Still need " << nMaxTemp << std::endl;
    assert(m_lpBufCur == m_lpBufMax);

    // read rest in buffer size chunks
    nTemp = nMaxTemp - (nMaxTemp % m_nBufSize);
    UINT nRead = 0;

    UINT nLeft = nTemp;
    UINT nBytes;
    do	{
      std::cout << "Will read " << nLeft << std::endl;
      nBytes =  fread(lpBuf, 1, nLeft, m_pFile);
      lpBuf  =  (BYTE*)lpBuf + nBytes;
      nRead  += nBytes;
      nLeft  -= nBytes;
    } while ((nBytes > 0) && (nLeft > 0));

    nMaxTemp -= nRead;

    if (nMaxTemp > 0) {
      // read last chunk into buffer then copy
      if (nRead == nTemp) {
	assert(m_lpBufCur == m_lpBufMax);
	assert(nMaxTemp < UINT(m_nBufSize));

	// fill buffer (similar to CArchive::FillBuffer, but no exception)
	if (!m_bDirectBuffer) {
	  UINT nLastLeft;
	  UINT nLastBytes;

	  if (!m_bBlocking)
	    nLastLeft = std::max(nMaxTemp, UINT(m_nBufSize));
	  else
	    nLastLeft = nMaxTemp;

	  BYTE* lpTemp = m_lpBufStart;
	  nRead = 0;
	  do {
	    nLastBytes =  fread(lpTemp, 1, nLastLeft, m_pFile);
	    lpTemp     =  lpTemp + nLastBytes;
	    nRead      += nLastBytes;
	    nLastLeft  -= nLastBytes;
	  } while ((nLastBytes > 0) && (nLastLeft > 0) && nRead < nMaxTemp);

	  m_lpBufCur = m_lpBufStart;
	  m_lpBufMax = m_lpBufStart + nRead;
	}
	else {
	}

	// use first part for rest of read
	nTemp = std::min(nMaxTemp, UINT(m_lpBufMax - m_lpBufCur));
	//Checked::memcpy_s(lpBuf, nMaxTemp, m_lpBufCur, nTemp);
	memcpy(lpBuf, m_lpBufCur, nTemp);
	m_lpBufCur += nTemp;
	nMaxTemp   -= nTemp;
      }
    }
  }
  return nMax - nMaxTemp;
}

void CArchive::Flush()
{
  if (IsLoading()) {
    // unget the characters in the buffer, seek back unused amount
    if (m_lpBufMax != m_lpBufCur)
      fseek(m_pFile, -(int(m_lpBufMax - m_lpBufCur)), SEEK_CUR);
    m_lpBufCur = m_lpBufMax;    // empty
  }
  else	{
    if (!m_bDirectBuffer) {
      // write out the current buffer to file
      if (m_lpBufCur != m_lpBufStart)
	fwrite(m_lpBufStart, 1, ULONG(m_lpBufCur - m_lpBufStart),m_pFile);
    }
    else {
    }
    m_lpBufCur = m_lpBufStart;
  }
}

void CArchive::FillBuffer(UINT nBytesNeeded)
{
  if(!IsLoading())
    return;
  
  
  UINT  nUnused       = UINT(m_lpBufMax - m_lpBufCur);
  ULONG nTotalNeeded = ((ULONG)nBytesNeeded) + nUnused;

  // fill up the current buffer from file
  if (!m_bDirectBuffer)	{
    assert(m_lpBufCur   != NULL);
    assert(m_lpBufStart != NULL);
    assert(m_lpBufMax   != NULL);
    
    if (m_lpBufCur > m_lpBufStart) {
      // copy unused
      if ((int)nUnused > 0) {
	// Checked::memmove_s(m_lpBufStart,
	//                    (size_t)(m_lpBufMax - m_lpBufStart), 
	//                    m_lpBufCur, nUnused);
	memmove(m_lpBufStart, m_lpBufCur, nUnused);
	m_lpBufCur = m_lpBufStart;
	m_lpBufMax = m_lpBufStart + nUnused;
      }

      // read to satisfy nBytesNeeded or nLeft if possible
      UINT nRead = nUnused;
      UINT nLeft;
      UINT nBytes;

      // Only read what we have to, to avoid blocking waiting on data 
      // we don't need
      if (m_bBlocking)  
	nLeft = nBytesNeeded-nUnused;
      else
	nLeft = m_nBufSize-nUnused;
      BYTE* lpTemp = m_lpBufStart + nUnused;
      do {
	nBytes =  fread(lpTemp, 1, nLeft, m_pFile);
	lpTemp =  lpTemp + nBytes;
	nRead  += nBytes;
	nLeft  -= nBytes;
      }
      while (nBytes > 0 && nLeft > 0 && nRead < nTotalNeeded);

      m_lpBufCur = m_lpBufStart;
      m_lpBufMax = m_lpBufStart + nRead;
    }
  }
  else {
    // seek to unused portion and get the buffer starting there
  }
  // not enough data to fill request?
  if ((ULONG)(m_lpBufMax - m_lpBufCur) < nTotalNeeded)
    return; 
}

void CArchive::WriteCount(ULONGLONG dwCount)
{
  if (dwCount < 0xFFFF)
    *this << (WORD)dwCount;  // 16-bit count
  else {
    *this << (WORD)0xFFFF;
    if (dwCount < 0xFFFFFFFF)
      *this << (DWORD)dwCount;  // 32-bit count
    else {
      *this << (DWORD)0xFFFFFFFF;
      *this << dwCount;
    }
  }
}

ULONGLONG CArchive::ReadCount()
{
  WORD wCount;
  *this >> wCount;
  if (wCount != 0xFFFF)
    return wCount;

  DWORD dwCount;
  *this >> dwCount;
  if (dwCount != 0xFFFFFFFF)
    return dwCount;

  ULONGLONG qwCount;
  *this >> qwCount;
  return qwCount;
}

template <typename T>
void take(CArchive& ar, size_t n) {
  for (size_t i = 0; i < n; i++) {
    T b;
    ar >> b;
    std::cout << b << std::endl;
  }
}

std::string str(CArchive& ar){
  int  s;
  int  n = AfxReadStringLength(ar, s);
  char buf[1024];
  std::cout << n << " chars" << std::endl;
  for (size_t i = 0;i < n; i++) ar >> buf[i];
  buf[n] = '\0';
  return std::string(&buf[0],int(n));
}
				  
int main()
{
  FILE* fp = fopen("PortStanley.gsn","rb");

  BYTE buf[1024];
    
  CArchive ar(fp,CArchive::load);
  ar >> buf[0] >> buf[1] >> buf[2] >> buf[3];
  buf[4] = '\0';
  std::cout << buf << std::endl;


  take<BYTE>(ar,4);
  take<WORD>(ar,4);
  take<DWORD>(ar,3);

  std::cout << str(ar) << std::endl;
    
  take<DWORD>(ar,1);
  
  std::cout << str(ar) << std::endl;
  std::cout << str(ar) << std::endl;
  std::cout << str(ar) << std::endl;
  
  take<WORD>(ar,5);
  take<DWORD>(ar,1);

  BOOL np;
  ar >> np;
  std::cout << np << std::endl;
  take<WORD>(ar,1);

  for 
  return 0;
}


  
    
    
