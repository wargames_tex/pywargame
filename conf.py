# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('.'))

import common
import vassal
import cyberboard
import latex
import sphinx_autorun
import inspect

# --- Some set-up ------------------------------------------------------------
builddir = os.environ['BUILDDIR']
if builddir == '':
    builddir = '.'


# -- Project information -----------------------------------------------------
project   = 'Python Wargame package'
copyright = '2023, Christian Holm Christensen'
author    = 'Christian Holm Christensen'
version   = '0.2'
release   = '0.2'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    #'sphinx.ext.autodoc',
    #'sphinx.ext.todo',
    #'sphinx.ext.coverage',
    'sphinx.ext.mathjax',
    #'sphinx.ext.ifconfig',
    'sphinx.ext.viewcode',
    #'sphinx.ext.napoleon',
    #'matplotlib.sphinxext.plot_directive',
    #'sphinx_autorun',
    'sphinx_automodapi.automodapi',
    'myst_parser',
    #'sphinx.ext.inheritance_diagram'
    #'autoapi.extension'
]
#autoapi_dirs     = ['common','vassal','cyberboard','latex']
#autoapi_root     = 'doc/api'
templates_path   = ['.templates']
language         = 'English'
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']
html_theme       = 'classic' # 'sphinxdoc'
html_static_path = ['.static']
master_doc       = 'index'
source_suffix    = ['.rst', '.md']

# -- Extension configuration -------------------------------------------------
automodapi_toctreedirnm        = builddir+ '/' + 'api'
automodapi_inheritance_diagram = True
autoclass_content              = 'both'
autodoc_class_signature        = 'separated'

#
# EOF
#
