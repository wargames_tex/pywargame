#!/usr/bin/env python
## BEGIN_IMPORTS
from sys import path
path.append('..')
from exporter    import GSNExporter
from common      import Verbose
from scenario    import Scenario
from extractor   import GSNExtractor
from head        import GBXHeader
## END_IMPORTS

# ====================================================================
if __name__ == '__main__':
    from argparse import ArgumentParser, FileType
    from pathlib import Path

    ap = ArgumentParser(description='Create draft VASSAL module')
    ap.add_argument('gsnfile',
                    help='The GSN file to data from',
                    type=FileType('rb'))
    ap.add_argument('-p','--patch',
                    help='A python script to patch generated module',
                    type=FileType('r'))
    ap.add_argument('-o','--output',
                    help='Output file to write module to',
                    type=str,
                    default='')
    ap.add_argument('-t','--title',
                    help='Override title',
                    type=str,
                    default=None)
    ap.add_argument('-v','--version',
                    help='Override version',
                    type=str,
                    default=None)
    ap.add_argument('-r','--rules',
                    help='Rules PDF file',
                    type=FileType('r'))
    ap.add_argument('-T','--tutorial',
                    help='Tutorial (v)log file',
                    type=FileType('r'))
    ap.add_argument('-W','--vassal-version',
                    help='Vassal version number',
                    type=str,
                    default='3.6.7')
    ap.add_argument('-V','--verbose',
                    help='Be verbose',
                    action='store_true')
    ap.add_argument('-G','--visible-grids',
                    action='store_true',
                    help='Make grids visible in the module')
    ap.add_argument('-X','--gamebox',
                    type=str,
                    default=None,
                    help='Override gamebox (.gbx) file')


    args = ap.parse_args()
    
    gsnname   = args.gsnfile.name
    zipfile   = None
    vmodname  = args.output
    rulesname = args.rules.name    if args.rules    is not None else None
    tutname   = args.tutorial.name if args.tutorial is not None else None
    # Read first 4 bytes from input to see if we're dealing with a GSN file
    magic     = args.gsnfile.read(4)
    args.gsnfile.close()
    if magic != GBXHeader.SCENARIO.encode():
        if magic[:2] == 'PK'.encode():
            from zipfile import ZipFile
            zipfile = ZipFile(gsnname,'r')
    # args.output.close()

    if vmodname == '':
        p        = Path(gsnname)
        vmodname = p.stem+'.vmod'
    
    patchname = args.patch.name if args.patch is not None else None
    if args.patch is not None:
        args.patch.close()

    Verbose().setVerbose(args.verbose)
        
    try:
        scenario  = None
        if zipfile is None:
            scenario = Scenario.fromFile(gsnname,args.gamebox)
        extractor = GSNExtractor(scenario,zipfile)
        exporter  = GSNExporter(extractor._d,
                                title         = args.title,
                                version       = args.version,
                                rules         = rulesname,
                                tutorial      = tutname,
                                visible       = args.visible_grids,
                                vassalVersion = args.vassal_version)
        exporter.run(vmodname,patchname)
    except Exception as e:
        from sys import stderr 
        print(f'Failed to build {vmodname}: {e}',file=stderr)
        from os import unlink
        try:
            unlink(vmodname)
        except:
            pass

        raise e

