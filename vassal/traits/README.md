# Traits 

## When adding new traits 

- Remember to register trait with 

	    Trait.known_traits.append(<NewTrait>)
		
- Remember to add source file to `vassal/Makfile` 

- Remember to add trait to imports in `vassal/traits/__init__.py` 

        from . <source> import <NewTrait>
		
- Remember to add trait to imports in `vassal/__init__.py` 

        from . traits.<source> import <NewTrait>
		
- Remember to add source file to `vassal/collect.py` 
