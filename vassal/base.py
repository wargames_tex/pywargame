# ====================================================================
# Key encoding
SHIFT = 65
CTRL = 130
ALT   = 520
CTRL_SHIFT = CTRL+SHIFT
ALT_SHIFT = ALT+SHIFT
NONE = '\ue004'
NONE_MOD = 0

# --------------------------------------------------------------------
def key(let,mod=CTRL):

    '''Encode a key sequence

    Parameters
    ----------
    let : str
        Key code (Letter)
    mod : int
        Modifier mask
    '''
    return f'{ord(let)},{mod}'

# --------------------------------------------------------------------
#
def hexcolor(s):
    if isinstance(s,str):
        s = s.replace('0x','')
        if len(s) == 3:
            r, g, b = [int(si,16)/16 for si in s]
        elif len(s) == 6:
            r = int(s[0:2],16) / 256
            g = int(s[2:4],16) / 256
            b = int(s[4:6],16) / 256
        else:
            raise RuntimeError('3 or 6 hexadecimal digits for color string')
    elif isinstance(s,int):
        r = ((s >> 16) & 0xFF) / 256
        g = ((s >>  8) & 0xFF) / 256
        b = ((s >>  0) & 0xFF) / 256
    else:
        raise RuntimeError('Hex colour must be string or integer')

    return rgb(int(r*256),int(g*256),int(b*256))
    
# --------------------------------------------------------------------
# Colour encoding 
def rgb(r,g,b):
    '''Encode RGB colour

    Parameters
    ----------
    r : int
        Red channel
    g : int
        Green channel
    b : int
        Blue channel

    Returns
    -------
    colour : str
        RGB colour as a string
    '''
    return ','.join([str(r),str(g),str(b)])

# --------------------------------------------------------------------
def rgba(r,g,b,a):
    '''Encode RGBA colour

    Parameters
    ----------
    r : int
        Red channel
    g : int
        Green channel
    b : int
        Blue channel
    a : int
        Alpha channel
    
    Returns
    -------
    colour : str
        RGBA colour as a string
    '''
    return ','.join([str(r),str(g),str(b),str(a)])

# --------------------------------------------------------------------
def dumpTree(node,ind=''):
    '''Dump the tree of nodes

    Parameters
    ----------
    node : xml.dom.Node
        Node to dump
    ind : str
        Current indent 
    '''
    print(f'{ind}{node}')
    for c in node.childNodes:
        dumpTree(c,ind+' ')

# --------------------------------------------------------------------
def registerElement(cls):
## BEGIN_IMPORT
    from . element import Element
## END_IMPORT
    
    Element.known_tags[cls.TAG] = cls
    
        
#
# EOF
#
