# ====================================================================
#
# Wrapper around a module 
#
class VMod:
    BUILD_FILE = 'buildFile.xml'
    MODULE_DATA = 'moduledata'
    
    def __init__(self,filename,mode):
        '''Interface to VASSAL Module (a Zip file)'''
        self._mode = mode
        self._vmod = self._open(filename,mode)

    def __enter__(self):
        '''Enter context'''
        return self

    def __exit__(self,*e):
        '''Exit context'''
        self._vmod.close()
        return None

    def _open(self,filename,mode):
        '''Open a file in VMod'''
        from zipfile import ZipFile, ZIP_DEFLATED

        return ZipFile(filename,mode,compression=ZIP_DEFLATED)
        
    def removeFiles(self,*filenames):
        '''Open a temporary zip file, and copy content from there to
        that file, excluding filenames mentioned in the arguments.
        Then close current file, rename the temporary file to this,
        and reopen in 'append' mode.  The deleted files are returned
        as a dictionary.

        Parameters
        ----------
        filenames : tuple
            List of files to remove from the VMOD

        Returns
        -------
        files : dict
            Dictionary from filename to content of the removed files.

        Note, the VMOD is re-opened in append mode after this
        '''
        from tempfile import mkdtemp
        from zipfile import ZipFile
        from shutil import move, rmtree 
        from os import path

        tempdir = mkdtemp()
        ret     = {}

        try:
            tempname = path.join(tempdir, 'new.zip')
            with self._open(tempname, 'w') as tmp:

                for item in self._vmod.infolist():
                    data = self._vmod.read(item.filename)

                    if item.filename not in filenames:
                        tmp.writestr(item, data)
                    else:
                        ret[item.filename] = data

            name = self._vmod.filename
            self._vmod.close()
            move(tempname, name)

            self._mode = 'a'
            self._vmod = self._open(name,'a')
        finally:
            rmtree(tempdir)

        # Return the removed files 
        return ret

    def fileName(self):
        '''Get name of VMod file'''
        return self._vmod.filename

    def replaceFiles(self,**files):
        '''Replace existing files with new files

        Parameters
        ----------
        files : dict
            Dictionary that maps file name to content
        '''
        self.removeFiles(*list(files.keys()))

        self.addFiles(**files);
    
    def addFiles(self,**files):
        '''Add a set of files  to this

        Parameters
        ----------
        files : dict
            Dictionary that maps file name to content.
        '''
        for filename,data in files.items():
            self.addFile(filename,data)

    def addFile(self,filename,content):
        '''Add a file to this

        Parameters
        ----------
        filename : str
            File name in module
        content : str
            File content
        
        Returns
        -------
        element : File
            The added element
        '''
        self._vmod.writestr(filename,content)

    def addExternalFile(self,filename,target=None):
        '''Add an external file element to this

        Parameters
        ----------
        kwargs : dict
            Dictionary of attribute key-value pairs
        
        Returns
        -------
        element : ExternalFile
            The added element
        '''
        if target is None: target = filename
        self._vmod.write(filename,target)
        
    def getFileNames(self):
        '''Get all filenames in module'''
        return self._vmod.namelist()

    def getFileMapping(self):
        '''Get mapping from short name to full archive name'''
        from pathlib import Path
        
        names = self.getFileNames()

        return {Path(p).stem: str(p) for p in names}
    
    def getFiles(self,*filenames):
        '''Return named files as a dictionary.

        Parameters
        ----------
        filenames : tuple
            The files to get 
        
        Returns
        -------
        files : dict
            Mapping of file name to file content
        '''
        fn  = self.getFileNames()
        ret = {}
        for f in filenames:
            if f not in fn:
                continue

            ret[f] = self._vmod.read(f)

        return ret

    def getDOM(self,filename):
        '''Get content of a file decoded as XML DOM

        Parameters
        ----------
        filename : str
            Name of file in module 
        '''
        from xml.dom.minidom import parseString

        r = self.getFiles(filename)
        if filename not in r:
            raise RuntimeError(f'No {filename} found!')

        return parseString(r[filename])
        
    def getBuildFile(self):
        '''Get the buildFile.xml decoded as a DOM tree'''
        return self.getDOM(VMod.BUILD_FILE)

    def getModuleData(self):
        '''Get the moduledata decoded as a DOM tree'''
        return self.getDOM(VMod.MODULE_DATA)

    def getInternalFile(self,filename,mode):
        return self._vmod.open(filename,mode)

    def addVSav(self,build):
        '''Add a `VSav` element to this

        Parameters
        ----------
        kwargs : dict
            Dictionary of attribute key-value pairs
        
        Returns
        -------
        element : VSav
            The added element
        '''
        return VSav(build=build,vmod=self)


#
# EOF
#
