## BEGIN_IMPORT
from common import VerboseGuard
from . base import *
from . element import Element
## END_IMPORT

# --------------------------------------------------------------------
class GlobalProperties(Element):
    TAG = Element.MODULE+'properties.GlobalProperties'
    def __init__(self,elem,node=None,**named):
        super(GlobalProperties,self).__init__(elem,self.TAG,node=node)
        
        for n, p in named:
            self.addProperty(n, **p)

    def getGame(self):
        return self.getParent(Game)
    def addProperty(self,**kwargs):
        '''Add a `Property` element to this

        Parameters
        ----------
        kwargs : dict
            Dictionary of attribute key-value pairs
        
        Returns
        -------
        element : Property
            The added element
        '''
        return GlobalProperty(self,node=None,**kwargs)
    
    def getProperties(self):
        return getElementsByKey(GlobalProperty,'name')

    def addScenarioTab(self,**kwargs):
        '''Add a scenario property tab

        Parameters
        -----------
        kwargs : dict
            Key-value pairs to send to ScenarioOptionsTab

        Returns
        -------
        element : ScenarioOptionsTab
            Added element
        '''
        return ScenarioOptionsTab(self,node=None,**kwargs)

    def getScenarioTabs(self):
        return getElementsByKey(ScenarioOptionsTab,'name')
    
registerElement(GlobalProperties)

# --------------------------------------------------------------------
class GlobalProperty(Element):
    TAG = Element.MODULE+'properties.GlobalProperty'
    def __init__(self,
                 elem,
                 node         = None,
                 name         = '',
                 initialValue = '',
                 isNumeric    = False,
                 min          = "null",
                 max          = "null",
                 wrap         = False,
                 description  = ""):
        super(GlobalProperty,self).__init__(elem,self.TAG,
                                            node         = node,
                                            name         = name,
                                            initialValue = initialValue,
                                            isNumeric    = isNumeric,
                                            min          = min,
                                            max          = max,
                                            wrap         = wrap,
                                            description  = description)

    def getGlobalProperties(self):
        return self.getParent(GlobalProperties)

registerElement(GlobalProperty)

# ====================================================================
class ScenarioOptionsTab(Element):
    TAG = Element.MODULE+'properties.ScenarioPropertiesOptionTab'
    LEFT = 'left'
    RIGHT = 'right'
    
    def __init__(self,elem,node=None,
                 name         = 'Options',
                 description  = 'Scenario options',
                 heading      = '',
                 leftAlign    = 'left',
                 reportFormat = ('!$PlayerId$ changed Scenario Option '
                                 '[$tabName$] $propertyPrompt$ from '
                                '$oldValue$ to $newValue$')):
        super(ScenarioOptionsTab,self).__init__(elem,
                                                self.TAG,
                                                node         = node,
                                                name         = name,
                                                description  = description,
                                                heading      = heading,
                                                leftAlign    = leftAlign,
                                                reportFormat = reportFormat)

    def addList(self,**kwargs):
        '''Add a list property option

        Parameters
        ----------
        kwargs : dict
            Key, value pairs to initalise ScenarioOptionList

        Returns
        -------
        element : ScenarioOption
            The added element
        '''
        return ScenarioOptionList(self,node=None,**kwargs)

    def addBoolean(self,**kwargs):
        '''Add a list property option

        Parameters
        ----------
        kwargs : dict
            Key, value pairs to initalise ScenarioOptionBool

        Returns
        -------
        element : ScenarioOption
            The added element
        '''
        return ScenarioOptionBool(self,node=None,**kwargs)

    def addString(self,**kwargs):
        '''Add a list property option

        Parameters
        ----------
        kwargs : dict
            Key, value pairs to initalise ScenarioOptionString

        Returns
        -------
        element : ScenarioOption
            The added element
        '''
        return ScenarioOptionString(self,node=None,**kwargs)

    def addNumber(self,**kwargs):
        '''Add a list property option

        Parameters
        ----------
        kwargs : dict
            Key, value pairs to initalise ScenarioOptionNumber

        Returns
        -------
        element : ScenarioOption
            The added element
        '''
        return ScenarioOptionString(self,node=None,**kwargs)

    def getOptions(self):
        return self.getElementsByKey(ScenarioOption,'name')

    def getListOptions(self):
        return self.getElementsByKey(ScenarioOptionList,'name')

    def getBoolOptions(self):
        return self.getElementsByKey(ScenarioOptionBool,'name')

    def getStringOptions(self):
        return self.getElementsByKey(ScenarioOptionString,'name')
    
    def getNumberOptions(self):
        return self.getElementsByKey(ScenarioOptionNumber,'name')
    
    def getGlobalProperties(self):
        return self.getParent(GlobalProperties)

registerElement(ScenarioOptionsTab)
    
# --------------------------------------------------------------------
class ScenarioOption(Element):

    def __init__(self,
                 tab, 
                 tag,
                 node         = None,
                 name         = '',
                 hotkey       = '',
                 description  = 'Set option value',
                 switch       = False,
                 initialValue = '',
                 **kwargs):
        '''
        Parameters
        ----------
        tab : ScenarioOptionsTab
            Tab to add to 
        tag : str
            Tag value (full)
        name : str
            Name of global property 
        hotkey : named-key
            Key stroke to send (global key)
        description : str
            Text to show user
        switch : bool
            If true, then prompt is put to the right
        initialValue : str
            Initial value
        kwargs : dict
            Other arguments
        '''
        super(ScenarioOption,self).__init__(tab,
                                            tag,
                                            node         = node,
                                            name         = name,
                                            hotkey       = hotkey,
                                            description  = description,
                                            switch       = switch,
                                            initialValue = initialValue,
                                            **kwargs)

    def getTab(self):
        return self.getParent(ScenarioOptionsTab)

# --------------------------------------------------------------------
class ScenarioOptionList(ScenarioOption):
    TAG = Element.MODULE+'properties.ListScenarioProperty'

    def __init__(self,
                 tab,
                 node         = None,
                 name         = '',
                 hotkey       = '',
                 description  = 'Set option value',
                 switch       = False,
                 initialValue = None,
                 options      = []):
        '''
        Parameters
        ----------
        tag : str
            Tag value (full)
        name : str
            Name of global property 
        hotkey : named-key
            Key stroke to send (global key)
        description : str
            Text to show user
        switch : bool
            If true, then prompt is put to the right
        initialValue : str
            Initial value.  If None, set to first option
        options : list 
            Possible values
        '''
        opts = ','.join([str(s) for s in options])
        if initialValue is None:
            initialValue = opts[0]
            
        super(ScenarioOptionList,self).__init__(tab,
                                                self.TAG,
                                                node         = node,
                                                name         = name,
                                                hotkey       = hotkey,
                                                description  = description,
                                                switch       = switch,
                                                initialValue = initialValue,
                                                options      = opts)

registerElement(ScenarioOptionList)

# --------------------------------------------------------------------
class ScenarioOptionBool(ScenarioOption):
    TAG = Element.MODULE+'properties.BooleanScenarioProperty'

    def __init__(self,
                 tab,
                 node         = None,
                 name         = '',
                 hotkey       = '',
                 description  = 'Set option value',
                 switch       = False,
                 initialValue = False):
        '''
        Parameters
        ----------
        tag : str
            Tag value (full)
        name : str
            Name of global property 
        hotkey : named-key
            Key stroke to send (global key)
        description : str
            Text to show user
        switch : bool
            If true, then prompt is put to the right
        initialValue : bool
            Initial value
        '''
        super(ScenarioOptionBool,self).__init__(tab,
                                                self.TAG,
                                                node         = node,
                                                name         = name,
                                                hotkey       = hotkey,
                                                description  = description,
                                                switch       = switch,
                                                initialValue = initialValue)
        
registerElement(ScenarioOptionBool)

# --------------------------------------------------------------------
class ScenarioOptionString(ScenarioOption):
    TAG = Element.MODULE+'properties.StringScenarioProperty'

    def __init__(self,
                 tab,
                 node         = None,
                 name         = '',
                 hotkey       = '',
                 description  = 'Set option value',
                 switch       = False,
                 initialValue = False):
        '''
        Parameters
        ----------
        tag : str
            Tag value (full)
        name : str
            Name of global property 
        hotkey : named-key
            Key stroke to send (global key)
        description : str
            Text to show user
        switch : bool
            If true, then prompt is put to the right
        initialValue : str
            Initial value
        '''
        super(ScenarioOptionString,self).__init__(tab,
                                                  self.TAG,
                                                  node         = node,
                                                  name         = name,
                                                  hotkey       = hotkey,
                                                  description  = description,
                                                  switch       = switch,
                                                  initialValue = initialValue)
        
registerElement(ScenarioOptionString)

# --------------------------------------------------------------------
class ScenarioOptionNumber(ScenarioOption):
    TAG = Element.MODULE+'properties.NumberScenarioProperty'

    def __init__(self,
                 tab,
                 node         = None,
                 name         = '',
                 hotkey       = '',
                 description  = 'Set option value',
                 switch       = False,
                 initialValue = False):
        '''
        Parameters
        ----------
        tag : str
            Tag value (full)
        name : str
            Name of global property 
        hotkey : named-key
            Key stroke to send (global key)
        description : str
            Text to show user
        switch : bool
            If true, then prompt is put to the right
        initialValue : str
            Initial value
        '''
        super(ScenarioOptionNumber,self).__init__(tab,
                                                  self.TAG,
                                                  node         = node,
                                                  name         = name,
                                                  hotkey       = hotkey,
                                                  description  = description,
                                                  switch       = switch,
                                                  initialValue = initialValue)

registerElement(ScenarioOptionNumber)

#
# EOF
#
