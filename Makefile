SPHINXOPTS	:=
SPHINXBUILD   	:= sphinx-build
SOURCEDIR     	:= .
BUILDDIR      	:= doc
SUBDIRS		= common		\
		  vassal		\
		  latex			\
		  cyberboard
SCRIPTS		= vassal/vassal.py 		\
		  cyberboard/cyberboard.py 	\
		  cyberboard/gbxextract.py 	\
		  cyberboard/gsnextract.py 	\
		  cyberboard/gsnexport.py 	\
		  cyberboard/gbx0pwd.py 	\
		  latex/wgexport.py 		


all:	
	$(foreach d, $(SUBDIRS), \
		$(MAKE) $(MAKEFLAGS) -C $(d) all;)
	mv $(SCRIPTS) .

clean:	
	rm -rf *~ __pycache__ public doc
	rm -f $(notdir $(SCRIPTS))
	rm -rf misc/__pycache__
	rm -f *.log *.aux *.synctex* logo.pdf info.json
	rm -f output.txt PortStanley.vmod 
	$(foreach d, $(SUBDIRS), \
		$(MAKE) $(MAKEFLAGS) -C $(d) clean;)

realclean: clean
	rm -f *.zip *.svg *.vmod *.gbx *.gsn *.gb_ *.gs_ *.png

sphinx-%:
	BUILDDIR=$(BUILDDIR) \
		$(SPHINXBUILD) -M $* \
			"$(SOURCEDIR)" \
			"$(BUILDDIR)" \
			$(SPHINXOPTS) 


doc:	sphinx-html

docker-prep:
	pip install -r requirements.txt

artifacts:	docker-prep all
	mkdir -p public
	cp $(notdir $(SCRIPTS)) public/
	cp requirements.txt public/
	cp README.md  public/


#
#
#
