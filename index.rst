.. LaTeX Wargame exporter documentation master file, created by
   sphinx-quickstart on Tue Nov 22 15:40:06 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Python  Wargame tool's documentation!
================================================

.. include:: README.md
   :parser: myst_parser.sphinx_
             
.. toctree::
   :maxdepth: 2
   :caption: Contents:


API Documentation
=================

.. automodapi:: common
.. automodapi:: vassal
.. automodapi:: cyberboard
.. automodapi:: latex
		

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
